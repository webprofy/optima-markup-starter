// Обработка отсутствующего console.log
if(window.console === undefined) {
	window.console = {
		log: function () {
			return false;
		}
	}
}

// Кроссбраузерный requestAnimationFrame
window.requestAnimFrame =
window.requestAnimationFrame       ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame    ||
window.oRequestAnimationFrame      ||
window.msRequestAnimationFrame     ||
function(callback, element){
	callback();
};

// Сообщаяем блокам о готовности API карт
ymapAPIready = false;

if(typeof ymaps !== 'undefined') {
	ymaps.ready(function () {
		ymapAPIready = true;
		$(document).trigger('ymapAPIready');
	});
}

// Предотвращаем всплытие блочных событий
$(function () {
	$('.bem').on('resize.block', function (e) {
		e.stopPropagation();
	});
});
//
// Узнаем ширину скролла
$(function getScrollBarWidth () {
var inner = document.createElement('p');
inner.style.width = '100%';
inner.style.height = '200px';

var outer = document.createElement('div');
outer.style.position = 'absolute';
outer.style.top = '0px';
outer.style.left = '0px';
outer.style.visibility = 'hidden';
outer.style.width = '200px';
outer.style.height = '150px';
outer.style.overflow = 'hidden';
outer.appendChild (inner);

document.body.appendChild (outer);
var w1 = inner.offsetWidth;
outer.style.overflow = 'scroll';
var w2 = inner.offsetWidth;
if (w1 == w2)
    w2 = outer.clientWidth;
document.body.removeChild (outer);


	$('head').append('<style type="text/css">.scroll-fixer{margin-right: -' + (w1 - w2) + 'px;overflow-y: scroll;}</style>');
	$('head').append('<style type="text/css">.scroll-fixer-pos{right: -' + (w1 - w2) + 'px;overflow-y: scroll;}</style>');
	$('head').append('<style type="text/css">html.fancybox-margin .fancyfixer{right: ' + (w1 - w2) + 'px;}</style>');
	// .fancyfixer на фиксированно спозиционированные элементы для ремона fancybox
});

// Предобработка ресайза, для отсечения лишних событий (в основном для мобил)
(function () {
	var windowWidth = $(window).width();

	$(window).on('resize', function () {
		var newWindowWidth = $(window).width();
		if(newWindowWidth != windowWidth) {
			windowWidth = newWindowWidth;
			$(window).trigger('resizeWidth');
		}
	});
})();

// Функции WP
var Webprofy = {
	toggleLabel : function($elements, enable){
		var disabled = !enable;
		$elements.each(function(){
			$(this).toggleClass('disabled', disabled).find('input').prop('disabled', disabled);
		});
		return this;
	},
	setInputStatus : function($elements, ok, message){
		var html = '<span class="status-' + (ok ? 'ok' : 'error') + '">' + (message || '') + '</span>';
		$elements.each(function(){
			$(this).closest('.field').find('.status').html(html);
		});
		return this;
	},
	oldIeCheck : $('html').hasClass('bx-ie9') || $('html').hasClass('bx-ie8'),

	isOldIe: function(){
		return this.oldIeCheck;
	}
};

function initSliders(){
	$('.bxslider').bxSlider({
		pager: false,
		adaptiveHeight: true
	});
}

sameheight = function(container){
	var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = [],
		$el,
		topPosition = 0;
	$(container).each(function() {
		$el = $(this);
		$($el).height('auto');
		topPostion = $el.position().top;

		if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
};

$(window).load(function() {
  sameheight('.is-sameheight');
});


$(window).resize(function(){
  sameheight('.is-sameheight');
});


$.fn.promoSlider = function(_options) {

	var options = $.extend({
		rotate: true,
		rotateSpeed: 5000,
		transitionTime: 1000,
		transition: function(olditem, newitem, step){
			newitem.css({'opacity': step});
		},
		transitionStart: function(olditem, newitem){
			// do nothing
		},
		transitionEnd: function(olditem, newitem){
			// do nothing
		}
	}, _options);

	if(typeof(this.data('promoSlider')) != "undefined"){
		return this.data('promoSlider');
	} else {
		return this.each(function(){

			var promo = $(this);
			var items = promo.find('.main-promo-item');
			var current = items.filter('.active');
			if(current.length != 1){
				current = items.eq(0);
			}

			var timeout;

			// Reset active and create pagination

			items.removeClass('active');
			current.addClass('active');
			promo.find('.promo-slider-paginator').remove();
			promo.append('<ul class="promo-slider-paginator"></ul>');
			var paginator = promo.find('.promo-slider-paginator');
			items.each(function(){
				if($(this).is('.active')){
					paginator.append('<li class="active"></li>');
				} else {
					paginator.append('<li></li>');
				}
			});

			// Set Height
			promo.find('.promo-slider').height(current.height());

			// Set Rotation
			if(options.rotate){
				timeout = setInterval(next, options.rotateSpeed);
			}

			function getNext(){
				var index = items.index(items.filter('.active'));
				index++;
				if(index >= items.length)
					index = 0;
				return index;
			}

			function getPrev(){
				var index = items.index(items.filter('.active'));
				index--;
				if(index < 0)
					index = items.length - 1;
				return index;
			}

			function switchTo(i){
				items.filter('.active').addClass('transition-from');
				items.eq(i).addClass('transition-to');
				var olditem = items.filter('.active');
				var newitem = items.eq(i);
				options.transitionStart(olditem, newitem);
				$({'step': 0}).animate({'step': 1}, {
					duration: options.transitionTime,
					step: function(val){
						options.transition(olditem, newitem, val);
					},
					complete: function(){
						options.transitionEnd(olditem, newitem);
						items.removeClass('active transition-from transition-to').eq(i).addClass('active');
						paginator.find('li').removeClass('active').eq(i).addClass('active');
					}
				});
			}

			function next(){
				switchTo(getNext());
			}
			function prev(){
				switchTo(getPrev());
			}

			var promoSlider = {
				next: next,
				prev: prev
			};

			$(this).data('promoSlider', promoSlider);
		});
	}
};

function initClickToChange(){
	$(document).on('click', '.click-to-change-link', function(e){
		e.preventDefault();
		var ctc = $(this).parents('.click-to-change');
		ctc.find('input[name="'+ctc.data('state-input')+'"]').val("1");
		ctc.find('.state-view').hide();
		ctc.find('.state-edit').show();
	});
	$(document).on('click', '.click-to-cancel-link', function(e){
		e.preventDefault();
		var ctc = $(this).parents('.click-to-change');
		ctc.find('input[name="'+ctc.data('state-input')+'"]').val("");
		ctc.find('.state-view').show();
		ctc.find('.state-edit').hide();
	});
}



function initSidebar(){
	$('header .menu-button').on('click', function(e){
		$('body').toggleClass('sidebar-opened');
		e.preventDefault();
	});
	$('.container').on('click', function(e){
		if($(e.target).is('.menu-button'))
			return;
		if($('body').is('.sidebar-opened')){
			$('body').removeClass('sidebar-opened');
		}
	});
}

function initDropdownMenu(){
	var WAIT_TIME = 500;

	function closeAll(){
		$('.header-dropdown-menu').removeClass('opened');
		$('header').removeClass('dropdown-opened');
		$('.header-menu a').find('span').css({'width': 0});
	}

	$('.header-menu a[data-dropdown-menu]').each(function(){
		var dropdown = $('#' + $(this).data('dropdown-menu'));
		var header = $(this).parents('header');

		$(this).on('mouseover', function(){
			if(!dropdown.is('.opened')){
				timestart = new Date().getTime();
				$(this).find('span').stop(true).animate({'width': '100%'}, WAIT_TIME, function(){
					$('.header-dropdown-menu').removeClass('opened');
					dropdown.addClass('opened');
					header.addClass('dropdown-opened');
					$('.header-menu a').find('span').css({'width': 0});
					$(this).css({'width': '100%'});
				});
			}
		});
		$(this).on('mouseout', function(){
			if(!dropdown.is('.opened')){
				$(this).find('span').stop(true).css({'width': '0'});
			}
		});
	});
	$('header').on('mouseleave', function(){
		closeAll();
	});
	$('header .menu-button').on('click', function(){
		closeAll();
	});
}

function initFixedHeader() {
	var headerBottom = $('.header-bottom');
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            headerBottom.addClass("fixed");
        }
        else {
            headerBottom.removeClass("fixed");
        }
    });
}

$.fn.selectRange = function(start, end) {
    if(!end) end = start;
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

function initFilter(){
	function numberWithSpaces(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	}
	function formatPrice(x, significant_digits, convert_factor) {

		if(!convert_factor){
			convert_factor = 1;
		}
		x = Math.round(x * convert_factor);

		var digits = x.toString().length;
		var divider = 1;
		if(digits > significant_digits){
			divider = Math.pow(10, digits - significant_digits);
		}

		x = x - (x % divider);
		return numberWithSpaces(x);
	}
	function bound(_number, _min, _max){
		return Math.max(Math.min(_number, _max), _min);
	}

	$('.bx_filter').each(function(){
		var CF = $(this);

		// Range control
		CF.find('.numbers-range-control').each(function(){
			var RC = $(this);
			var log = (RC.data('log') == '1');
			var field_min = RC.find('.range-min');
			var field_max = RC.find('.range-max');
			var realfield_min = $('#' + field_min.data('name') );
			var realfield_max = $('#' + field_max.data('name') );
			var data_min = RC.data('min');
			var data_max = RC.data('max');
			var significant_digits = RC.data('significant-digits') | 3;
			var convert_factor = RC.data('convert-factor') | 1;

			console.log("Convert factor", convert_factor);

			function updateRealFields(){
				realfield_min.val(field_min.val().toString().replace(/[^\d]/g, '') / convert_factor);
				realfield_max.val(field_max.val().toString().replace(/[^\d]/g, '') / convert_factor);
				realfield_min.change();
				realfield_max.change();
			}

			function updateFields(values){
				if(log){
					var min = Math.exp(values[0]);
					var max = Math.exp(values[1]);
					min = bound(min, data_min, data_max);
					max = bound(max, data_min, data_max);
					field_min.val(formatPrice(min, significant_digits, convert_factor));
					field_max.val(formatPrice(max, significant_digits, convert_factor));
				} else {
					field_min.val(formatPrice(values[0], significant_digits, convert_factor));
					field_max.val(formatPrice(values[1], significant_digits, convert_factor));
				}
				updateRealFields();
			}

			function updateSlider(onchange){
				var min = field_min.val().toString().replace(/[^\d]/g, '') / convert_factor;
				var max = field_max.val().toString().replace(/[^\d]/g, '') / convert_factor;

				min = parseInt(min, 10);
				max = parseInt(max, 10);

				if(isNaN(min) || isNaN(max))
					return;

				if(onchange){
					if(max < min) {
						max = Math.round(min * 1.1);
					}
					min = bound(min, data_min, data_max);
					max = bound(max, data_min, data_max);

				}

				field_min.val(numberWithSpaces(min * convert_factor));
				field_max.val(numberWithSpaces(max * convert_factor));

				updateRealFields();

				if(log){
					RC.find('.range-slider').slider({"values": [Math.log(min), Math.log(max)], range: true,
						min: min,
						max: max});
				} else {
					RC.find('.range-slider').slider({"values": [min, max], range: true,
						min: min,
						max: max});
				}
			}

			if(parseInt(data_min) == 0 && log){
				data_min = 1; // Logaryphmic slider can't handle values < 1
			}
			if(parseInt(data_max) == 0){
				console.log('Zero price slider');
				return;
			}

			var options = {
				range: true,
				max: data_max,
				min: data_min,
				values: [RC.data('left'), RC.data('right')],
				step: 1,
				slide: function(e, ui){
					updateFields(ui.values);
				}
			}

			if(log){
				options.step = 0.00001;
				options.max = Math.log(options.max) + options.step;
				options.min = Math.log(options.min) - options.step;
				options.values = [Math.log(options.values[0]), Math.log(options.values[1])];
			}

			RC.find('.range-slider').slider(options);
			updateFields(options.values);

			/* RC.find('.range-min, .range-max').on('keydown', function(e){
				if(e.which > 57){
					e.preventDefault();
					return false;
				}
			}); */

			RC.find('.range-min, .range-max').on('keyup', function(e){
				console.log(e.which);
				if(e.which > 47 || e.which == 8){
					 // store current positions in variables
					var start = this.selectionStart;
					var l = this.value.length;
					updateSlider();
					if(this.value.length != l){
						start += (this.value.length - l);
					}
					this.setSelectionRange(start, start);
				}
			});
			RC.find('.range-min, .range-max').on('change blur', function(e){
				updateSlider(true);
			});
		});
	});
}

function customSliderItemsSlideOut(items, step){
	var TIMEALIVE = 0.3;
	var length = items.length;
	var timestep = (1 - TIMEALIVE) / length;

	items.each(function(key){
		var resampledstep = (step - timestep * key) / TIMEALIVE;
		if(resampledstep < 0)
			resampledstep = 0;
		if(resampledstep > 1)
			resampledstep = 1;
		$(this).css({
			'opacity': (1 - resampledstep),
			'left': (- resampledstep * 200) + 'px'
		});
	});
}

function customSliderItemsSlideIn(items, step){
	var TIMEALIVE = 0.4;
	var length = items.length;
	var timestep = (1 - TIMEALIVE) / length;

	items.each(function(key){
		var resampledstep = (step - timestep * key) / TIMEALIVE;
		if(resampledstep < 0)
			resampledstep = 0;
		if(resampledstep > 1)
			resampledstep = 1;
		$(this).css({
			'opacity': resampledstep,
			'top': ((1 - resampledstep) * 200) + 'px'
		});
	});
}

function customSliderTransition(olditem, newitem, step){
	if(step < 0.33333){
		step = step * 3; // map back to 0-1
		customSliderItemsSlideOut(olditem.find('.promo-text > div'), step);
	} else if (step < 0.66666){
		step = (step-0.33333) * 3; // map back to 0-1
		newitem.css({'opacity': (step-0.25) * 2});
	} else {
		step = (step-0.66666) * 3; // map back to 0-1
		//newitem.find('.promo-text').css({'opacity': (step-0.75) * 4});
		customSliderItemsSlideIn(newitem.find('.promo-text > div'), step);
	}
}

function initForms(){
	// Checkboxes
	$(document).find('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
		$(this).iCheck();
		$(this).on('ifToggled', function(e){
			$(this).trigger('change').trigger('click'); // Trigger default event
		});
	});

	$(document).on('comparePageReloaded', function(){
		$('#bx_catalog_compare_block').find('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
			$(this).iCheck();
			$(this).on('ifToggled', function(e){
				$(this).trigger('change').trigger('click'); // Trigger default event
			});
		});
	});
}

function initOrderForm(){
	$('.order-page').each(function(){
		var $page = $(this),
			payway = new function(){
				var $payway = $page.find('.payway-holder'),
					$radios = $payway.find('.payway-radio');

				$.extend(this, {
					enableForDelivery : function(delivery){
						var changeValue = false;
						$radios.each(function(){
							var $radio = $(this),
								deliveryValues = eval($radio.attr('data-deliveries')),
								disabled = ($.inArray(delivery, deliveryValues) == -1);
							Webprofy.toggleLabel($radio.closest('label'), !disabled);
							if(disabled && $radio.is(':checked')){
								changeValue = true;
								$radio.prop('checked', false);
							}
						});

						if(changeValue){
							$radios.filter(':enabled:first').prop('checked', true);
						}
					}
				});
			},
			delivery = new function(){
				var $holder = $page.find('.delivery-holder').eq(0),
					$showPickup = $page.find('.show-pickup'),
					$hidePickup = $page.find('.hide-pickup'),
					pickupValues = eval($holder.attr('data-pickup-values')),
					$radios = $holder.find('.delivery-radio').change(function(){
						var $radio = $(this),
							value = $radio.val(),
							isPickup = $.inArray(value, pickupValues) > -1;
						$showPickup.toggle(isPickup);
						$hidePickup.toggle(!isPickup);
						payway.enableForDelivery(value);
					}),
					$radio = $radios.filter(':checked');

				if(!$radio.length){
					$radio = $radios.eq(0).prop('checked', true);
				}
				$radio.trigger('change');
			}
	});
}

var wpCart = function(){
	var self = this;
	this.showPopupItemAdded = function(item){
		$.fancybox(
			Mustache.render(this.popupTemplate, {
				item: item,
				cart_url: "/cart/",
				messages: self.messages
			})
		);
		BX.onCustomEvent(window,'OnBasketChange');
	};

	this.popupTemplate = '' +
	'<div class="item-added-to-cart">' +
		'<div class="item-added-action-title">{{messages.item_added_to_cart}}</div>' +
		'<div class="item-added-image"><img src="{{item.image}}" /></div>' +
		'<div class="item-added-title">{{item.title}}</div>' +
		'<div class="buttons">' +
			'<a href="#" class="close button">{{messages.continue_shopping}}</a> ' +
			'<a href="{{cart_url}}" class="button go-to-cart">{{messages.go_to_cart}}</a>' +
		'</div>' +
	'</div>';

	this.messages = {
		item_added_to_cart: 'Товар добавлен в корзину',
		continue_shopping: 'Продолжить покупки',
		go_to_cart: 'Перейти в корзину'
	};
};

// Вариант с зумом
function initCatalogGallery(){
	$('.image-gallery-slider-with-thumbnails').each(function(){
		var gallery = $(this);
		var items = gallery.find('.more-images li');
		var container = gallery.find('.image');
		var image = container.find('img');

		if(image.data('fullimage') != image.attr('src')){
			image.addClass('fancyzoom');
		}

		items.on('click', function(){
			if(!$(this).is('.active')){
				$(this).addClass('active').siblings().removeClass('active');
				var img = $(this).find('img');
				container.addClass('loading');
				image.one('load', function(){
					container.removeClass('loading');
				});
				image.attr('src', img.data('image'));
				image.attr('data-fullimage', img.data('fullimage'));

				if(image.data('fullimage') != image.attr('src')){
					image.addClass('fancyzoom');
				} else {
					image.removeClass('fancyzoom');
				}
			}
		});
	});

	$(document).on('mouseover', '.image-gallery-slider-with-thumbnails .image img.fancyzoom', function(e){
		$('.fancyview-zoom').remove();
		var zoom = $(this).clone();
		zoom.attr('src', zoom.data('fullimage'));
		zoom.insertAfter($(this)).wrap('<div class="fancyview-zoom"></div>');

		$(document).one('mouseout', '.image-gallery-slider-with-thumbnails .image', function(e){
			//console.log('remove');
			$('.fancyview-zoom').remove();
		});

	});
	$(document).on('mousemove', '.image-gallery-slider-with-thumbnails .image img.fancyzoom', function(e){
		if($('.fancyview-zoom').length == 0){
			$(this).mouseover();
		}

		if(!e.offsetX){
			posx = e.pageX-$(this).offset().left;
			posy = e.pageY-$(this).offset().top;
		} else {
			posx = e.offsetX;
			posy = e.offsetY;
		}

		percentx = posx / $(this).parent().width();
		percenty = posy / $(this).parent().height();

		$('.fancyview-zoom').css({
			left: (posx - $('.fancyview-zoom').width() / 2) + 'px',
			top: (posy - $('.fancyview-zoom').height() / 2) + 'px'
		});

		$('.fancyview-zoom img').css({
			left: ($('.fancyview-zoom').width() / 2 - percentx * $('.fancyview-zoom img').width()) + 'px',
			top: ($('.fancyview-zoom').height() / 2 - percenty * $('.fancyview-zoom img').height()) + 'px'
		});
	});
}

// Вариант без зума
function initCatalogGallery2(){
	$('.image-gallery-slider-with-thumbnails').each(function(){
		var gallery = $(this);
		var items = gallery.find('.more-images li');
		var container = gallery.find('.image');
		var image = container.find('img');

		if(image.data('fullimage') != image.attr('src')){
			image.addClass('fancyzoom');
		}

		items.on('click', function(){
			if(!$(this).is('.active')){
				$(this).addClass('active').siblings().removeClass('active');
				var img = $(this).find('img');
				container.addClass('loading');
				image.one('load', function(){
					container.removeClass('loading');
				});
				image.attr('src', img.data('image'));
				image.attr('data-fullimage', img.data('fullimage'));

				/* if(image.data('fullimage') != image.attr('src')){
					image.addClass('fancyzoom');
				} else {
					image.removeClass('fancyzoom');
				} */
			}
		});

		image.on('click', function(){
			var links = [];
			items.each(function(){
				links.push($(this).find('img').data('fullimage'));
			});
			var index = items.index(items.filter('.active'));
			links = links.concat(links.splice(0,index));
			$.fancybox(links);
		});

	});
}

$(function(){
	// initSliders();
	// initClickToChange();
	// initShowOnCondition();
	// initSidebar();
	// initDropdownMenu();
	// initFilter();
	// initForms();
	// initOrderForm();

	initCatalogGallery();

	$('.main-promo').promoSlider({
		transition: customSliderTransition,
		transitionStart: function(olditem, newitem){
			newitem.css({opacity: 0});
			//newitem.find('.promo-text').css({'opacity': 1});
			newitem.find('.promo-text > div').css({'opacity': 0, 'top': '200px'});
		},
		transitionEnd: function(olditem, newitem){
			//olditem.find('.promo-text').css({'opacity': 1});
			olditem.find('.promo-text > div').css({'opacity': 1, 'left': 0});
			//newitem.find('.promo-text').css({'opacity': 1});
		},
		transitionTime: 2000
	});

	$('.catalog-sort-and-view select').each(function(){
		$(this).select2({
			minimumResultsForSearch: 20
		});
	});
	$('.bx_filter select').select2({
		minimumResultsForSearch: 20
	});
});

function startPreventBodyScroll(){
	$('body').addClass('is-fixed');
	if($(window).width() < 768){
		$('body').css({'position': 'fixed', width: $('body').width() + 'px'});
	}
	
}
function endPreventBodyScroll(){
	$('body').removeClass('is-fixed');
	if($(window).width() < 768){
		$('body').css({'position': 'static', 'width': 'auto'});
	}
}

// Переносит картинки в бэкграунд родителя
function jsBgImage(){
	$('.js-bg-image').each(function(){
		$this = $(this);
		$src = $this.attr('src');
		if($this.data('at-2x') && window.devicePixelRatio > 1.5){
			$src = $(this).data('at-2x');
		}
		$this.parent().css({ 'background-image': 'url("' + $src + '")'});
		$this.hide();
	});
}

$(function(){
	jsBgImage();
});

// Прижимаем футер
$(function () {
	if($('html').hasClass('is-footer-fixed')) {
		var body = $('body');
		var footer = $('.layout__footer');
		var footerHeight = footer.height() + parseInt(footer.css('margin-top'));
		body.css('margin-bottom', footerHeight);
	}
});



$(function () {
	// Модальные окна
	$('body').on('click', 'a.pupop, .js-modal', function (e) {
		var target = $(this).data('href') || $(this).attr('href');
		var additionalTitle = $(this).attr("data-additional-title");
		var wrapCSS = $(this).attr("data-wrap-css");

		if(!additionalTitle || typeof additionalTitle == 'undefined')
		{
			additionalTitle = '';
		}
		if(!wrapCSS || typeof wrapCSS == 'undefined')
		{
			wrapCSS = '';
		}
		
		if(target.substr(0,1) == '#'){
			var $target = $(target);
		} else {
			$.get(target, function(data){
				$target = $(data);
				open(additionalTitle);
			});
			return false;
		}

		if($.fancybox.isOpen) {
			$.fancybox.close(true);
			setTimeout(function() {open(additionalTitle);}, 250);
		} else {
			open(additionalTitle);
		}
		
		

		function open(additionalTitle) {
			$.fancybox.open(
				$target,
				{
					padding: 0,
					margin: 20,
					closeEffect: 'none',
					wrapCSS: 'is-default' + ' ' + wrapCSS,
					closeSpeed: 0,
					openSpeed: 0,
					openEffect: 'none',
					openOpacity: false,
					closeOpacity: false,
					fitToView: true,
					scrolling: 'visible',

					beforeShow: function () {
						$('html').addClass('fancybox-margin fancybox-lock');
						$('.fancybox-wrap').livequery(function(){
							var $context = $(this);
							var goodField = $context.find(".good-name");

							var checkVal = function ($input, regExp) {
								var result = regExp.test($input.val());
								if (result) {
									$input.closest('.form-standart__field').removeClass('is-error');
								} else {
									$input.closest('.form-standart__field').addClass('is-error');
								}

								return result;
							};
							$('[data-mask="phone"]', $context).each(function(){
								$(this).mask('+7 (999) 999-99-99');
							});
							$('[data-mask="email"]', $context).change(function () {

								var regExp = /^([A-Za-z0-9_-]+\.)*[A-Za-z0-9\+_-]+@[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*\.[a-z]{2,6}$/g;
								checkVal($(this), regExp);
								return true;
							});
							
							if(additionalTitle)
							{
								var formTitle = $(".form-standart__title", $context);
								formTitle.text(formTitle.text() + ' ' + additionalTitle);
							}
							
							if(goodField.length > 0 && additionalTitle)
							{
								goodField.val(additionalTitle);
							}
						});
					}
				}
			);
		}

		return false;
	});

	// Модальные окна ajax
	$('.js-modal-ajax').on('click',  function() {
		var href =  $(this).attr('href');
		// console.warn('.js-modal-ajax '+href);
		$.fancybox.open(
			{
				href: href
			},
			{
				wrapCSS: 'is-ajax',
				type: 'ajax',
				fitToView: true,
				autoResize: true,
				padding: 20,
				margin: 20,
				maxWidth: 1000,
				afterLoad: function (current, previous) {
					var $content = $(current.content);

					$content.addClass('_ajax-append');
					current.content = $('<div>').append($content.clone()).html();
					$.fancybox.showLoading();
				},
				afterShow: function () {
					$('.fancybox-wrap').livequery();
					setTimeout(function () {
						$('.fancybox-wrap .bem').trigger('resize.block');
						$.fancybox .hideLoading();
						$('.fancybox-wrap').removeClass('is-ajax');
					}, 600);
				}
			}
		);
		return false;
	});

});

// Плейсхолдеры
$(function () {
	$('input[placeholder], textarea[placeholder]').placeholder();
});

// Стилизация селектов
$(function () {
	$('select.select2').each(function () {
		var placeholder = $(this).attr('placeholder');
		var templateSR = function (state) {
			var $thstate = $(state.element);

			var $state = $(
				'<span>' + state.text  + '</span>'
			);
			if($thstate.data('img')){
				$('<span class="is-img-point" style="background-image: url('+ $thstate.data('img') +')"></span>').prependTo($state);
			}
			if($thstate.data('format')) $state.addClass('is-format-'+ $(state.element).data('format'));
			return $state;
		};

		$(this).select2({
			minimumResultsForSearch: 8,
			placeholder: placeholder,
			templateResult: templateSR,
			templateSelection: templateSR
		});
	});
});


// Стилизация радиокнопок и чекбоксов
$(function () {
	$('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
		$(this).iCheck();
		$(this).on('ifToggled', function(e){
			$(this).trigger('change').trigger('click'); // Trigger default event
		});
	});
});

// Скрипт для форм form.result.new
$.fn.initWebForm = function(block, options) {
/* 
WP_SUCCESS_MODE

S1 - Редирект на страницу "Спасибо"
S2 - Редирект на текущую страницу (перезагрузка страницы)
S3 - Редирект на текущую страницу с показом информационного попапа, после загрузки страницы
S4 - Замена содержимого формы на сообщение об успешной отправке без изменения размера формы
S5 - Замена содержимого формы на сообщение об успешной отправке с изменением размера формы
S6 - Замена содержимого формы на сообщение об успешной отправке с последующим полным скрытием формы после небольшой паузы (закрыванием попапа).
S7 - Показ сообщения об успешной отправке в попапе и очистка (сброс) формы.
S8 - Показ сообщения об успешной отправке в попапе БЕЗ очистки (сброса) формы.
*/

	var default_options = {
		WP_SUCCESS_MODE: 'S5'
	};

	options = $.extend({}, default_options, options);

	var $form = $(this);
	var blockName = $form.find('input[name=block_name]').val() || block || 'form-standart';
	var $context = $form.closest('.'+blockName);

	var $inputs = $form.find('[data-fieldname]');
	var $fields = $('.' + blockName + '__field', $context);
	var $successMessage = $('.success-message-modal',$context);
	var isAjax = $context.hasClass('js-ajax') || $form.hasClass('js-ajax');

	var MESS = {
		'browser': 'Браузер',
		'windowsize': 'Размер окна',
		'utm_source': 'UTM источник',
		'utm_campaign': 'UTM кампания',
		'utm_medium': 'UTM медиа',
		'utm_keyword': 'UTM ключевые слова',
		'location': 'Текущая страница',
		'message_sent': 'Сообщение отправлено'
	};

	if($form.data('form-initiated') !== undefined) return;
	$form.data('form-initiated', true);

	prepareFieldsValidation();
	initFormSubmit();

	function getFieldsData(){
		var data = {};
		$form.find('[data-fieldname]').each(function(){
			data[$(this).data('fieldname')] = $(this).val();
		});
		return data;
	}

	function getCookie(name) {
	  var matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	function getUtms() {
		var utm = getCookie('__utmz');
		utms = {};
		if(utm){
			eval(utm.replace(/.*?(utm.*?)=([^|]*)[|]?/g, "utms['$1'] = '$2';\n"));
		}
		return utms;
	}

	function getUserInfo(){
		var userinfo = {};
		userinfo['browser'] = navigator.userAgent;
		userinfo['windowsize'] = $(window).width() + "×" + $(window).height();
		userinfo['location'] = document.location.href;
		utms = getUtms();
		if(utms.utmcsr){
			userinfo['utm_source'] = utms.utmcsr;
		}
		if(utms.utmccn){
			userinfo['utm_campaign'] = utms.utmccn;
		}
		if(utms.utmcmd){
			userinfo['utm_medium'] = utms.utmcmd;
		}
		if(utms.utmcmd){
			userinfo['utm_keyword'] = utms.utmctr;
		}

		var stringresult = "";
		$.each(userinfo, function(key, value){
			stringresult += MESS[key] + ": " + value + "\n";
		});
		return stringresult;
	}

	function prepareFieldsValidation () {
		// Обеспечиваем совместимость с предыдущим решением
		$fields.filter('[data-necessary]').each(function () {
			var $field = $(this);
			var $inputs = $('input, textarea, select', $field);

			$inputs.each(function () {
				var $this = $(this);
				$this.removeAttr('required').data('required', true);

				$this.on('click keydown change', function() {
					$(this).closest('.'+blockName+'__field').removeClass('is-none');
				});
			});
		});

		$('input[data-necessary], textarea[data-necessary], select[data-necessary]', $form).each(function () {
			var $this = $(this);

			$this.data('required', true);

			$this.on('click keydown change', function() {
				$(this).closest('.'+blockName+'__field').removeClass('is-none');
			});
		});

		// Меняем дефолтное поведение обязательных полей
		$('input[required], textarea[required], select[required]', $form).each(function () {
			var $this = $(this);

			$this.removeAttr('required').data('required', true);

			$this.on('click keydown change', function() {
				$(this).closest('.'+blockName+'__field').removeClass('is-none');
			});
		});
	}

	function initFormSubmit () {
		if(isAjax) {
			$form.ajaxForm({
				dataType: (Webprofy.isOldIe() ? 'text' : 'json'),
				beforeSerialize: function(){
					$form.find('input[name="confirm"]').remove();
					$form.find('input[data-fieldname="_utm"]').val(getUserInfo());
				},
				beforeSubmit: function(){
					var preValidation = checkFields();

					/* Очищаем все ошибки формы*/
					var $all_error = $('.'+blockName+'__common-error-placeholder', $context);
					if($all_error.length) $all_error.html('');
					if(preValidation) {
						$form
							.find('input[type="submit"]')
							.prop( "disabled", true)
							.addClass('is-disabled');
					}

					return preValidation;
				},
				success: ajaxSuccessHandler,
				error: ajaxErrorHandler
			});
		} else {
			$form.on('submit', checkFields);
		}
	}

	function ajaxSuccessHandler (data) {
		//console.log(data);

		$form.find('input[type="submit"]').prop( "disabled", false).removeClass('is-disabled');
		$form.find('.error-text').remove();
		$form.find('.is-error').removeClass('is-error');

		if(Webprofy.isOldIe()) {
			eval('var data=' + data);
		}

		// console.log(data);

		if(data.status == 'error'){
			var errorCommonContainer = $('.'+blockName+'__common-error-placeholder', $context);
			$.each(data.errors, function(key, value){
				var $input = $form.find('[data-fieldname="' + key + '"]');
				var $container = $input.closest('.'+blockName+'__field');
				var $error;

				if($container.length > 0){
					$error = $container.find('.'+blockName+'__error');
					if($error.length==0)
						$error = $('<div class="'+blockName+'__error"></div>').appendTo($container);

					$error.text(value);
					$container.addClass('is-error');

					$input.on('change',function(){
						$container.removeClass('is-error');
					});

				} else {
					if(errorCommonContainer.length > 0){
						$container = errorCommonContainer;
					}else{
						$form.append('<div class="'+blockName+'__common-error-placeholder"></div>');
						errorCommonContainer = $('.'+blockName+'__common-error-placeholder', $context);
					}

					$(errorCommonContainer).append(
						'<div class="'+blockName+'__common-error"><div class="'+blockName+'__common-error-text">'  + value + '</div></div>'
					);
				}
			});
			$(document).trigger('webform.error', [$form.attr('name'), data, getFieldsData()]);
		}

		if(data.status == 'success'){
			$(document).trigger('webform.success', [$form.attr('name'), getFieldsData()]);
			//console.log(options.WP_SUCCESS_MODE);
			//console.log(options);

			if (data.message == undefined) {
				data.message = options.WP_DEFAULT_SUCCESS_MESSAGE;
			}

			switch (options.WP_SUCCESS_MODE) {
				case 'S1':
					if(data.redirect) {
						window.location = data.redirect;
					} else {
						window.location.reload();
					}
				    break;
				case 'S2':
					window.location.reload();
				    break;
				case 'S3':
					var loc = window.location;
					if(window.location.href.indexOf('?') + 1) {
						var popup_get = '&' + options.WP_FORM_ID + '=Y';
					} else {
						var popup_get = '?' + options.WP_FORM_ID + '=Y';
					}
					window.location = window.location.href + popup_get;
				    break;
				case 'S4':
					// доработать - Замена содержимого формы на сообщение об успешной отправке без изменения размера формы
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S5':
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S6':
					// доработать - Замена содержимого формы на сообщение об успешной отправке с последующим полным скрытием формы после небольшой паузы (закрыванием попапа)
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S8':
					/** Используется в Личном кабинете */
					$.fancybox.close();
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
					break;
				case 'S7':
				default:
					$.fancybox.close();
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
							$context.find('form').trigger('reset'); // todo: доработать - сброс формы на дефолтное состояние
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
				    break;

			}

			/*
				if(data.redirect){
					document.location = data.redirect;
				} else if(data.message) {
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				} else {
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
				}
			*/
		}
	}

	function ajaxErrorHandler (data) {
	//	console.log(data);
		
		if(typeof window.isLocalBuild !== 'undefined') {
			data.status = 'success';
			ajaxSuccessHandler(data);
		} else {
			$form.find('input[type="submit"]').prop( "disabled", false).removeClass('disabled');
			$form.find('.is-captcha-field').addClass('is-error');
		}
	}

	function checkFields () {
		var result = true;

		$fields.each(function () {
			var $field = $(this);
			var $inputs = $('input, textarea, select', $field);
			if($field.hasClass('is-error') || $field.hasClass('is-none')) {
				result = false;
				return;
			}

			$inputs.each(function () {
				var $input = $(this);
				var checkResult = true;
				var isContinue = true;

				if($input.data('required') == undefined) return;

				switch($input.attr('type')) {
					case 'checkbox':
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($inputs.filter(':checked').length == 0) {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;

					case 'radio':
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($inputs.filter(':checked').length == 0) {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;

					default:
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($input.val() == '') {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;
				}
			});
		});

		return result;
	}
};

//Обновляем последние просмотренные товары
function updateViewList(site_id, product_id, parent_id) {
	var href="/bitrix/components/bitrix/catalog.element/ajax.php";
	$.ajax({
		type: "POST",
		url: href,
		dataType: "text",
		data: {
			AJAX: "Y",
			SITE_ID: site_id,
			PRODUCT_ID: product_id,
			PARENT_ID: parent_id
		}
	}).done(function(resp) {
		//Thanks, bitrix
		eval('var response='+resp);
		if(response.STATUS == 'SUCCESS') {
			// console.log('updateViewList.ok');
		}else{
			console.warn('updateViewList.error');
		}
	});
}

// контент зависимый от чекбокса .show-on-condition
function initShowOnCondition(){
	$('[data-watch]').each(function(){
		var $this = $(this),
			watch = $this.attr('data-watch'),
			$that = $(watch);

		var change = $this.attr('data-change');
		if(change){
			$that.change(function(){
				try{
					eval(change);
				}
				catch(e){
					console.error('bad data-change');
				}
			}).eq(0).trigger('change');
			return;
		}

		var showSelectEquals = $this.attr('data-show-select-equals');
		if(showSelectEquals){
			$this.change(function(){
				$that.filter(':checked').val('');
			});
		}
	});

	var listeners = [];

	$('.show-on-condition').each(function(){
		var self = $(this);
		var condition = self.data('condition');

		// Complex parser :)
		var conditions = condition.split(" and ");

		$.each(conditions, function(key, value){
			var operands = value.split(" ");
			if(operands.length == 3){
				listeners.push({
					input: operands[0],
					conditions: conditions,
					object: self
				});
			}
		});
	});

	$(document).on('change', 'input, textarea, select', function(){
		var name = $(this).attr('name');
		updateConditions(name);
	});

	function updateConditions(name){
		$.each(listeners, function(key, value){

			if(typeof(name) === 'string' && value.input != name)
				return; // skip this one;

			var result = true;
			$.each(value.conditions, function(k, v){
				var operands = v.split(" ");
				if(operands.length == 3){
					var input = $('[name="' + operands[0] + '"]');
					if(input.length >= 1){
						var input_value;
						if(input.length == 1){
							input_value = input.val();
						} else {
							input_value = input.filter(':checked').val();
						}

						if(input_value == operands[2]){
							result = result & (operands[1] == "is");
						} else {
							result = result & (operands[1] == "not");
						}
					} else {
						result = false;
					}
				} else {
					result = false;
				}
			});
			if(result){
				value.object.show();
			} else {
				value.object.hide();
			}
		});
	}
	updateConditions();
}
$(function(){
	initShowOnCondition();
});

$(function(){
	// Content table wrapper
	$('.content-area table').each(function(){
		$(this).wrap('<div class="layout__content-table"></div>');
	});
});

// Layout-overflow
// Фиксит горизонтальный скролл из-за неверной ширины экрана с учётом скроллбара
$(function(){
	function recalculateOverflow(){
		if(window.innerWidth < 1024){
			// Не применимо для малых разрешений
			$('.layout__overflow[style]').removeAttr('style');
		} else {
			var ratio = document.body.clientWidth * 100 / window.innerWidth;
			var margin = ratio / -1.995;
			$('.layout__overflow').each(function(){
				$(this).css({
					'width': ratio + 'vw',
					'margin-left': margin + 'vw'
				});
			});
		}
	}
	if(document.body.clientWidth < window.innerWidth){
		recalculateOverflow();
		$(window).on('resize', recalculateOverflow);
	}
});

function plural(n,f){n%=100;if(n>10&&n<20)return f[2];n%=10;return f[n>1&&n<5?1:n==1?0:2]}
