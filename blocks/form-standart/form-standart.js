// Обработка стандартной формы
	$(function () {
		$('.form-standart').livequery(function () {
			var $context = $(this);
			var $form = $('form', $context);

			$form.initWebForm('form-standart');

			$('[data-mask="phone"]', $context).each(function(){
				$(this).mask('+7 (999) 999-99-99');
			});

			// Появление полей для смены пароля
			$('.js-pass-link', $context).on('click', function () {
				$context.find('[name=changepassword]').val('1');
				$('.js-pass-change', $context).addClass('is-visible');

				$(this).closest('.form-standart__field').addClass('is-hidden');
				return false;
			});

			$('.js-pass-cancel', $context).on('click', function () {
				$context.find('[name=changepassword]').val('');
				$('.js-pass-change', $context).removeClass('is-visible');
				$('.js-pass-link', $context).closest('.form-standart__field').removeClass('is-hidden');
				return false;
			});
		});
	});
