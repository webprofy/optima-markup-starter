$(function(){
	$('.header-mobile').livequery(function(){
		var $context = $(this);
		var $buttons = $('a[data-action="mobile-menu"]', $context);
		var isMenuOpened = false;

		function closeActiveMenus(isSwitch){
			$buttons = $('a[data-action="mobile-menu"]', $context); // dom changes, so buttons must be set again
			$buttons.filter('.is-active').each(function(){
				$(this).removeClass('is-active');
				$($(this).attr('href')).removeClass('is-active');
			});
			if(!isSwitch){
				$context.removeClass('is-menu-opened');
				isMenuOpened = false;
				window.endPreventBodyScroll();
			}	
		}
		$(document).on('click', 'a[data-action="mobile-menu"]', function(e){
			var $button = $(this);
			var $plate = $($button.attr('href'));
			e.preventDefault();
			if($button.is('.is-active')){
				closeActiveMenus();
			} else {
				closeActiveMenus(true);
				$button.addClass('is-active');
				$plate.addClass('is-active');

				if($plate.height() >= $(window).height()*0.8) {
					$plate.addClass('is-fullscreen');
				} else {
					$plate.removeClass('is-fullscreen');
				}
				
				$context.addClass('is-menu-opened');
				isMenuOpened = true;

				window.startPreventBodyScroll();
			}
		});
		$(document).on('touchstart', function(e){
			if(isMenuOpened && $(e.target).closest('.header-mobile').length === 0){
				closeActiveMenus();
			}
		});
		$(document).on('cartDataAvailable', function(e){
			if(isMenuOpened){
				$('.header-mobile__cart-button, .header-mobile__cart-holder').addClass('is-active');
			}
		});
		$(document).on('fancybox-before-show', function(){
			closeActiveMenus();
		});
	});
});